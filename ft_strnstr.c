/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 11:45:58 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 13:57:07 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s, const char *f, size_t size)
{
	size_t i;
	size_t j;

	i = 0;
	j = 0;
	if (!f[0])
		return ((char*)s);
	while (s[i] && i < size)
	{
		j = 0;
		while (f[j] == s[i + j] && f[j] && s[i + j])
			j++;
		if (f[j] == 0 && i + j <= size)
			return ((char*)s + i);
		i++;
	}
	return (NULL);
}
