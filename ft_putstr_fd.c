/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/20 10:32:22 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/11 12:26:38 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	str_len(const char *s)
{
	size_t		len;

	len = 0;
	while (s && s[len])
		len++;
	return (len);
}

void			ft_putstr_fd(char *s, int fd)
{
	write(fd, s, str_len(s));
}
