/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 13:24:31 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 14:12:01 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	i;
	size_t	l;
	int		test;

	i = 0;
	test = 0;
	l = size;
	if (size > ft_strlen(dest))
		l = ft_strlen(dest);
	while (l + 1 < size && src[i] && (test = 1))
		dest[l++] = src[i++];
	if (test == 1 && dest != NULL)
		dest[l] = 0;
	l += ft_strlen(&src[i]);
	return (l);
}
