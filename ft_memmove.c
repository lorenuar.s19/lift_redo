/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 11:05:34 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 13:40:45 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t size)
{
	size_t	i;
	char	*tmp_dst;
	char	*tmp_src;

	tmp_dst = (char*)dst;
	tmp_src = (char*)src;
	if (src < dst)
	{
		i = size;
		while (i--)
			tmp_dst[i] = tmp_src[i];
	}
	else if (src > dst)
	{
		i = 0;
		while (i < size)
			tmp_dst[i++] = *tmp_src++;
	}
	return (dst);
}
