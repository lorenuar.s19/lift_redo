/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 16:00:56 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 13:41:45 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t size)
{
	size_t	i;

	i = 0;
	while (i < size)
	{
		*(char*)&dst[i] = *(char*)&src[i];
		if (*(char *)&src[i] == (char)c)
			return (&dst[i + 1]);
		i++;
	}
	return (NULL);
}
