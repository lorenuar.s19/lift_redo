/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 14:24:51 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 13:46:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		**free_all(char **tab, int y)
{
	while (--y >= 0)
		free(tab[y]);
	free(tab);
	return (NULL);
}

static int		lento_sep(char const *s, char c)
{
	int		i;

	i = 0;
	while (s && s[i] && s[i] != c)
		i++;
	return (i);
}

static int		count_words(char const *s, char c)
{
	int count;
	int pos;

	count = 1;
	if (s[0] == c)
		count = 0;
	pos = 0;
	while (s[pos] && s[pos + 1])
	{
		if (s[pos] == c && s[pos + 1] != c)
			count++;
		pos++;
	}
	return (count);
}

static char		*tmp_str(const char *s, char c)
{
	int		i;
	char	*tmp;

	i = 0;
	if ((tmp = malloc(sizeof(char) * (lento_sep(s, c) + 1))) != 0)
	{
		while (s[i] != c && s[i] != '\0')
		{
			tmp[i] = s[i];
			i++;
		}
		tmp[i] = '\0';
		return (tmp);
	}
	return (NULL);
}

char			**ft_split(char const *s, char c)
{
	int		i;
	int		y;
	char	**split;

	i = 0;
	y = 0;
	if (s == NULL)
		return (NULL);
	if (!(split = malloc(sizeof(char *) * (count_words(s, c) + 1))))
		return (NULL);
	while (s[i] != '\0')
	{
		if (s[i] != c)
		{
			if (!(split[y] = tmp_str(&s[i], c)))
				return (free_all(split, y));
			i = i + lento_sep(&s[i], c);
			y++;
		}
		else if (s[i] == c)
			i++;
	}
	split[y] = NULL;
	return (split);
}
