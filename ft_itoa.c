/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/20 13:32:53 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 13:33:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_charsize_int(int n)
{
	int		size;

	size = 0;
	if (n <= 0)
	{
		n *= -1;
		size++;
	}
	while (n > 0)
	{
		n /= 10;
		size++;
	}
	return (size);
}

char			*ft_itoa(int n)
{
	char		*a;
	int			len;
	int			tmp;

	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	tmp = n;
	len = ft_charsize_int(n);
	if (!(a = malloc(sizeof(char) * (len + 1))))
		return (NULL);
	a[len] = '\0';
	if (n < 0)
		n *= -1;
	while (len-- > 0)
	{
		a[len] = n % 10 + '0';
		n /= 10;
	}
	if (tmp < 0)
		a[0] = '-';
	return (a);
}
