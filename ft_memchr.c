/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 10:25:09 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 13:41:40 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t size)
{
	size_t			i;
	char			*tmps;

	i = 0;
	tmps = 0;
	if (size != 0)
	{
		tmps = (char*)s;
		while (i < size && tmps[i])
		{
			if (tmps[i] == c)
				return (&tmps[i]);
			i++;
		}
		if (tmps[i] == c && tmps[i] == '\0')
			return (&tmps[i]);
	}
	return (NULL);
}
