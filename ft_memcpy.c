/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 10:42:32 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 13:40:44 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t size)
{
	size_t	i;

	i = 0;
	if (dst == src)
		return (dst);
	while (i < size)
	{
		if (*(char *)&dst[i] != *(char *)&src[i])
			*(char *)&dst[i] = *(char *)&src[i];
		i++;
	}
	return (dst);
}
